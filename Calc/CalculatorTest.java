package Calc;

import com.apple.eawt.Application;
import org.junit.jupiter.api.*;
import org.omg.CORBA.portable.ApplicationException;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    static Calculator calculator;

    @BeforeAll
    public static void before() {
        System.out.println("before()");
        calculator = new Calculator();
    }

    @AfterAll
    public static void after() {
        System.out.println("after()");
        calculator.clear();
    }

    @Test
    @DisplayName("Add 2 numbers")
    void testAddition() {
        int result = calculator.addition(2, 3);
        int expected = 5; // 2 + 3 = 5
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Subtract 2 numbers")
    void testSubtraction() {
        int result = calculator.subtraction(2, 3);
        int expected = -1; // 2 - 3 = -1
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Divide 2 numbers")
    public void testDivision() {
        calculator.division(5, 2);
    }

    // Expected in JUnit 5
    @Test
    @DisplayName("Divide by 0")
    public void testDivideByZero() {
        Assertions.assertThrows(ArithmeticException.class, () -> {
            calculator.division(5, 0);
        });
    }


    




}