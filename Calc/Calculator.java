package Calc;

public class Calculator {

    private int ans;

    public Calculator() {
        this.ans = 0;
    }

    public int addition(int a, int b) {
        ans = a + b;
        return ans;
    }

    public int subtraction(int a, int b) {
        ans = a - b;
        return ans;
    }

    public int addition(int value) {
        ans += value;
        return ans;
    }

    public int subtraction(int value) {
        ans -= value;
        return ans;
    }

    public int getAns() {
        return ans;
    }

    public void clear() {
        ans = 0;
    }

    public int division(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("You can't divide by 0");
        }

        ans =  a / b;
        return ans;
    }

//    public int multiply(int a, int b) {
//        return a * b;
//    }
}
