package Calc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class CalculatorParamTest {

    @Parameters
    public static Iterable<Object[]> getData() {
        List<Object[]> obj = new ArrayList<>();
        obj.add(new Object[] {3,1,4});
        obj.add(new Object[] {2, 3, 5});
        obj.add(new Object[] {3, 3, 6});
        return obj;
    }

    private int a, b, exp;
    public CalculatorParamTest(int a, int b, int exp) {
        this.a = a;
        this.b = b;
        this.exp = exp;
    }

    @Test
    public void additionTest() {
        Calculator calculator = new Calculator();
        int result = calculator.addition(a, b);
        assertEquals(exp, result);
    }

}